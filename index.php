<?php include "../includes/header.html"; ?>
<title>lainring</title>
<style>
.banner{
	display: inline-block;
	margin: 1%;
	width: 240px;
	height: 60px;
}
</style>

<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">

<h1> Lainring </h1>
<p>Lainring is a decentralized webring created by the users of Lainchan, an anonymous image board. </p>

<p> For lainanons: I maintain my copy with git. <a href="https://gitlab.com/binrc/lainring">Link to lainring git repo</a>. If you would like a static copy, you can view <a href="static.php">the simple html version</a>. </p>

<p> <b>Membership in this webring does not imply endorsement of any other website in the webring. </b></p>


<?php
// turn off warnings for undefined array keys so I can see actualy real issues
error_reporting(E_ALL);
//error_reporting(E_ERROR | E_PARSE | E_NOTICE);
$json = file_get_contents("lainring.json");

if($json === false){
	echo "cannot open lainring.json";
	exit;
}

$data = json_decode($json, true);

if($data === null){
	echo "cannot decode lainring.json";
	exit;
}

/* does not work
$gitref = trim(file_get_contents(".git/refs/heads/master"), "\n\r\t\v\x00");
$gitlog = file_get_contents(".git/logs/HEAD");
$commit = NULL;

foreach(preg_split("/((\r?\n)|(\r\n?))/", $gitlog) as $line){
	if(strpos($line, $gitref) !== false){
		$commit = $line;
	}
}


printf("<p> Git commit: <code>%s</code></p>", $commit);

$sliced = explode(" ", $commit);

$timestamp= $sliced[4];

printf("<p> Last updated: %s</p>", gmdate("Y-m-d\TH:i:s+00:00", $timestamp));
 */

printf("<p> Last updated: %s</p>", gmdate("Y-m-d\TH:i:s+00:00", $data["updated"]));

printf("<h2>www</h2>\n");

foreach($data['items'] as $key => $value){
	if($value['online'] == "true"){
		if(array_key_exists('locale', $value)){
			if($value['title'] !== null){
				$lc = $value['title'] . " [" . $value['locale'] . "]";
			} else {
				$lc = $value['url'] . " [" . $value['locale'] . "]";
			}
		} else {
			if($value['title'] !== null){
				$lc = $value['title'];
			} else {
				$lc = $value['url'];
			}
		}
		printf("<a href='%s' title='%s'><img class='banner' src='images/%s' alt='%s'></a>\n", $value['url'], htmlentities($lc), $value['img'], htmlentities($lc));
	}
}


printf("<h2>RSS feeds</h2>\n");
printf("<p><a href='opml.php'>OPML format</a></p>\n");
printf("<p>URL list for newsboat: </p>\n");
printf("<pre><code>");
foreach($data['items'] as $key => $value){
	if($value['online'] == "true"){
		if(array_key_exists('feed', $value)){
			printf("%s\n", $value['feed']);
		}
	}
}
printf("</code></pre>\n");


printf("<h2>Tor</h2>\n");
foreach($data['items'] as $key => $value){
	if($value['online'] == "true"){
		if(array_key_exists("tor", $value)){
			printf("<a href='%s'><img class='banner' src='images/%s' alt='%s'></a>", $value['tor'], $value['img'], $value['title']);
		}
	}
}

printf("<h2>i2p</h2>\n");
foreach($data['items'] as $key => $value){
	if($value['online'] == "true"){
		if(array_key_exists("i2p", $value)){
			printf("<a href='%s'><img class='banner' src='images/%s' alt='%s'></a>", $value['i2p'], $value['img'], $value['title']);
		}
	}
}

printf("<h2>Offline, abandoned, domain sniped, etc</h1>");
foreach($data['items'] as $key => $value){
	if($value['online'] == "false" ){
		if(array_key_exists('locale', $value)){
			if($value['title'] !== null){
				$lc = $value['title'] . " [" . $value['locale'] . "]";
			} else {
				$lc = $value['url'] . " [" . $value['locale'] . "]";
			}
		} else {
			if($value['title'] !== null){
				$lc = $value['title'];
			} else {
				$lc = $value['url'];
			}
		}
		printf("<a href='%s' title='%s'><img class='banner' src='images/%s' alt='%s'></a>\n", $value['url'], htmlentities($lc), $value['img'], htmlentities($lc));
	}
}


?>
</div>
</div>
<?php include "../includes/footer.html"; ?>
