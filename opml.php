<?php
header("Content-Type: application/octet-stream");
header("Content-Transfer-Encoding: Binary");
header("Content-disposition: attachment; filename=\"lainring-feeds.opml\"");
$json = file_get_contents("lainring.json");

if($json === false){
	exit;
}

$data = json_decode($json, true);

if($data === null){
	exit;
}


printf("<?xml version='1.0' encoding='UTF-8'?>\n");
printf("<opml version='1.1'>\n");
printf("<head><title>Lainring</title></head><body>\n");
foreach($data['items'] as $key => $value){
	if($value['online'] == true){
		if(array_key_exists("feed", $value)){
			$t = str_replace("'", "", $value['title']);
			$t = str_replace('"', '', $t);
	printf("<outline title='%s' text='%s' type='rss' xmlUrl='%s'/>\n", $t, $t, htmlentities($value['feed']));
		}
	}
}

printf("</body></opml>");
?>
