<?php
die;
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>lainring</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
/* FORCE BANNER SIZES */
.banner{
	width: 240px;
	height: 60px;
}

/* BOILERPLATE FOR PRE>CODE BLOCKS */
pre, code{
	font-family: monospace;
	max-width: 240px;
}
code{
	font-size: 14px;
}
pre{
	border: 1px solid black;
	padding: 1%;
	overflow: auto;
	font-size: 12px;
}

/* RESPONSIVE */
*{
	box-sizing: border-box;
}

[class*="col-"]{
	float: left;
		padding: 15px;
}

.row::after{
	content: "";
		clear: both;
		display: table;
}

[class*="col-"]{ /* mobile phones */
	width: 100%;
}
@media only screen and (min-width: 768px){
	/* For desktop: */
	.col-1{width: 8.33%;}
		.col-2{width: 16.66%;}
		.col-3{width: 25%;}
		.col-4{width: 33.33%;}
		.col-5{width: 41.66%;}
		.col-6{width: 50%;}
		.col-7{width: 58.33%;}
		.col-8{width: 66.66%;}
		.col-9{width: 75%;}
		.col-10{width: 83.33%;}
		.col-11{width: 91.66%;}
		.col-12{width: 100%;}
}
.offline{
	filter: saturate(0);
}

.offline:hover{
	filter: none;
}
.site-box{
	margin: 1%;
	display: inline-block;
}
p{
	overflow-wrap: break-anywhere;
}
</style>

<?php
//suppress warnings
error_reporting(E_ALL ^ E_WARNING); 

// spoof user agent
ini_set('user_agent', 'Mozilla/5.0 (X11; Linux x86_64; rv:124.0) Gecko/20100101 Firefox/124.0');
//echo ini_get('user_agent');

function help(){
	echo "USAGE:
		-h\tprint this message
		-c\tcheck clearnet sites
		-t\tcheck tor (must provide a proxy port)
		-i\tcheck i2p (must provide a proxy port)
		-p 1234\tproxy port
";

	die;
}

function getjson(){
	$json = file_get_contents("lainring.json");
	
	if($json === false){
		exit;
	}
	
	$data = json_decode($json, true);
	
	if($data === null){
		exit;
	}

	return $data;
	
}

function clearnet($dump, $html){
	$data=getjson();
	foreach($data['items'] as $key => $value){
		$header = get_headers($value['url']);
		$server = null;
		$status = null;
		$location = null;
		if(!$header || count($header) === 0){
			$status = "offline";
		} else {
			$code = $header[0];
			
			$j = count($header);

			// iterate through all headers, redirects exist

			for($i=0; $i<$j; $i++){
				if(str_starts_with($header[$i], 'Server: ')){
					$server = $header[$i];
				}
				if(str_starts_with($header[$i], 'HTTP/1.1 200 OK')){
					$status = "online";
				}
				if(str_starts_with($header[$i], 'Location: ')){
					$location = $header[$i];
				}
			}
		}


		// printer
		if(!$html){
			printf("%s - %s", $value['url'], $status);
	
			if($location !== null){
				printf(" - redirects to %s", $location);
			}
			if($server !== null){
				printf(" - Running on %s", $server);
			}
	
			printf("\n");

			if($dump){
				print_r($header);
			}
		} else {
			if($status == null){
				$status = "offline";
			}
			printf("<div class='site-box'>");
			printf("<a href='%s'><img class='banner %s' src='images/%s' alt='%s'></a>", $value['url'], $status, $value['img'], $value['title']);

			printf("<pre>");
			if($status !== null){
				printf("%s\n", $status);
			}

			if($location !== null){
				printf("redirects to %s\n", $location);
			} else {
				printf("\n");
			}
			if($server !== null){
				printf("%s\n", $server);
			} else {
				printf("\n");
			}

			printf("</pre></div>");
		}





	}
	//printf("<a href='%s'><img class='banner' src='images/%s' alt='%s'></a>", $value['url'], $value['img'], $value['title']);
}



function overlay($protocol, $proxyport, $dump){

}

// get cli args
/*
var_dump($argc);
var_dump($argv);
 */
$protocol == null;
$proxyport == null;

if($argc == 1){
	help();
}

$i = 1; // argv[0] is name of the program
for($i; $i<$argc; $i++){
	if($argv[$i] == "-c"){
		$protocol="clearnet";
	} elseif($argv[$i] == "-t"){
		$o == "tor";
	} elseif($argv[$i] == "-i"){
		$o == "i2p";
	} elseif($argv[$i] == "-p"){
		$proxyport=$argv[$i+1];
		$i++;
	}
}

if($protocol=="clearnet"){
	clearnet(0,1);
} else {
	overlay($protocol, $proxyport);
}



/* does not work
$gitref = trim(file_get_contents(".git/refs/heads/master"), "\n\r\t\v\x00");
$gitlog = file_get_contents(".git/logs/HEAD");
$commit = NULL;

foreach(preg_split("/((\r?\n)|(\r\n?))/", $gitlog) as $line){
	if(strpos($line, $gitref) !== false){
		$commit = $line;
	}
}


printf("<p> Git commit: <code>%s</code></p>", $commit);

$sliced = explode(" ", $commit);

$timestamp= $sliced[4];

printf("<p> Last updated: %s</p>", gmdate("Y-m-d\TH:i:s+00:00", $timestamp));
 */

/*
printf("<h2>www</h2>\n");

foreach($data['items'] as $key => $value){
	printf("<a href='%s'><img class='banner' src='images/%s' alt='%s'></a>", $value['url'], $value['img'], $value['title']);
}

printf("<h2>RSS feeds</h2>\n");
printf("<p><a href='opml.php'>OPML format</a></p>\n");
printf("<p>URL list for newsboat: </p>\n");
printf("<pre><code>");
foreach($data['items'] as $key => $value){
	if( $value['feed'] !== null){
		printf("%s\n", $value['feed']);
	}
}
printf("</code></pre>\n");



printf("<h2>Tor</h2>\n");
foreach($data['items'] as $key => $value){
	if($value['tor'] !== null){
		printf("<a href='%s'><img class='banner' src='images/%s' alt='%s'></a>", $value['tor'], $value['img'], $value['title']);
	}
}

printf("<h2>i2p</h2>\n");
foreach($data['items'] as $key => $value){
	if($value['i2p'] !== null){
		printf("<a href='%s'><img class='banner' src='images/%s' alt='%s'></a>", $value['i2p'], $value['img'], $value['title']);
	}
}
 */

?>

</div>
</div>
</body>
</html>
