<!DOCTYPE html>
<html lang="en">
<head>
<title>lainring</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
/* FORCE BANNER SIZES */
.banner{
	display: inline-block;
	margin: 1%;
	width: 240px;
	height: 60px;
}

/* BOILERPLATE FOR PRE>CODE BLOCKS */
pre, code{
	font-family: monospace;
}
code{
	font-size: 14px;
}
pre{
	border: 1px solid black;
	padding: 1%;
	overflow: auto;
	font-size: 12px;
}

/* RESPONSIVE */
*{
	box-sizing: border-box;
}

[class*="col-"]{
	float: left;
	padding: 15px;
}

.row::after{
	content: "";
	clear: both;
	display: table;
}

[class*="col-"]{ /* mobile phones */
	width: 100%;
}
@media only screen and (min-width: 768px){
	/* For desktop: */
	.col-1{width: 8.33%;}
	.col-2{width: 16.66%;}
	.col-3{width: 25%;}
	.col-4{width: 33.33%;}
	.col-5{width: 41.66%;}
	.col-6{width: 50%;}
	.col-7{width: 58.33%;}
	.col-8{width: 66.66%;}
	.col-9{width: 75%;}
	.col-10{width: 83.33%;}
	.col-11{width: 91.66%;}
	.col-12{width: 100%;}
}
</style>
</head>
<body>

<div class="row">
<div class="col-12">

<h1> Lainring </h1>
<p>Lainring is a decentralized webring created by the users of Lainchan, an anonymous image board. </p>

<p> For lainanons: I maintain my copy with git. <a href="https://gitlab.com/binrc/lainring">Link to lainring git repo</a>. </p>

<p> <b>Membership in this webring does not imply endorsement of any other website in the webring. </b></p>

<?php
$json = file_get_contents("lainring.json");

if($json === false){
	exit;
}

$data = json_decode($json, true);

if($data === null){
	exit;
}

printf("<p> Last updated: %s</p>\n", gmdate("Y-m-d\TH:i:s+00:00", $data["updated"]));

printf("<h2>www</h2>\n");

foreach($data['items'] as $key => $value){
	if($value['online'] == "true"){
		printf("<a href='%s'><img class='banner' src='https://0x19.org/lainring/images/%s' alt=\"%s\"></a>\n", $value['url'], $value['img'], $value['title']);
	}
}

printf("<h2>RSS feeds</h2>\n");
printf("<p>URL list for newsboat: </p>\n");
printf("<pre><code>");
foreach($data['items'] as $key => $value){
	if(array_key_exists("feed", $value) && $value['online'] == "true"){
		printf("%s\n", $value['feed']);
	}
}
printf("</code></pre>\n");


printf("<h2>Tor</h2>\n");
foreach($data['items'] as $key => $value){
	if(array_key_exists('tor', $value) && $value['online'] == "true"){
		printf("<a href='%s'><img class='banner' src='https://0x19.org/lainring/images/%s' alt=\"%s\"></a>\n", $value['tor'], $value['img'], $value['title']);
	}
}

printf("<h2>i2p</h2>\n");
foreach($data['items'] as $key => $value){
	if(array_key_exists("i2p", $value) && $value['online'] == "true"){
		printf("<a href='%s'><img class='banner' src='https://0x19.org/lainring/images/%s' alt=\"%s\"></a>\n", $value['i2p'], $value['img'], $value['title']);
	}
}

printf("<h2>Offline, abandoned, domain sniped, etc</h1>");
foreach($data['items'] as $key => $value){
	if($value['online'] == "false"){
		printf("<a href='%s'><img class='banner' src='https://0x19.org/lainring/images/%s' alt=\"%s\"></a>\n", $value['url'], $value['img'], $value['title']);
	}
}
?>

</div>
</div>
</body>
</html>
